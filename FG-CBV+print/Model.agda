{-# OPTIONS --prop --rewriting #-}

module FG-CBV+print.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import FG-CBV+print.Algebra

M : ∀ {i} → Set i → Set i
M A = List ℕ × A

[]-idr : ∀ {i} {A : Set i} (ns : List A) → ns ++ [] ≡ ns
[]-idr [] = refl
[]-idr (x ∷ ns) = ap (x ∷_) ([]-idr ns)

List-ass : ∀ {i} {A : Set i} (ns ms ps : List A) → ns ++ ms ++ ps ≡ ns ++ (ms ++ ps)
List-ass [] ms ps = refl
List-ass (x ∷ ns) ms ps = ap (x ∷_) (List-ass ns ms ps)

postulate
  funext : ∀ {i}{A B : Set i}{f g : A → B} →
    ((a : A) → f a ≡ g a) → f ≡ g

model : Algebra
model = record
  { Con = Set
  ; Ty = Set
  ; Sub = λ Γ Δ → Γ → Δ
  ; Val = λ Γ A → Γ → A
  ; Tm = λ Γ A → Γ → M A
  ; _⇒_ = λ A B → A → M B
  ; T = λ A → ↑p 𝟙 → M A
  ; Bool = 𝟚
  ; ∙ = ↑p 𝟙
  ; _▹_ = _×_
  ; id = idf
  ; _∘_ = _∘f_
  ; _,_ = λ δ a γ → δ γ ,Σ a γ
  ; p = π₁
  ; ε = const ↑[ * ]↑
  ; _[_]v = _∘f_
  ; _[_]t = _∘f_
  ; q = π₂
  ; return = λ a γ → [] ,Σ (a γ)
  ; _to_ = λ ma mb γ → let (ns ,Σ a) = ma γ
                           (ns' ,Σ b) = mb (γ ,Σ a)
                       in ns ++ ns' ,Σ b
  ; print_then_ = λ n ma γ → n ∷ π₁ (ma γ) ,Σ π₂ (ma γ)
  ; thunk = λ ma γ → const (ma γ)
  ; force = λ ma γ → ma γ ↑[ * ]↑
  ; lam = λ mb γ a → mb (γ ,Σ a)
  ; app = λ mb γa → mb (π₁ γa) (π₂ γa)
  ; true = const I
  ; false = const O
  ; itev = λ b u v γ → if b γ then u γ else v γ
  ; itet = λ b u v γ → if b γ then u γ else v γ
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ∙η = refl
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; [id]v = refl
  ; [id]t = refl
  ; [∘]v = refl
  ; [∘]t = refl
  ; toβ = refl
  ; toη = λ {Γ A t} → funext λ γ → ap (_,Σ π₂ (t γ)) ([]-idr _)
  ; toass = λ {Γ A B C t u v} → funext λ γ → ap
              (_,Σ π₂ (v (γ ,Σ π₂ (u (γ ,Σ π₂ (t γ))))))
              (List-ass _ (π₁ (u (γ ,Σ π₂ (t γ))))
                (π₁ (v (γ ,Σ π₂ (u (γ ,Σ π₂ (t γ)))))))
  ; printβ = refl
  ; Tβ = refl
  ; Tη = refl
  ; ⇒β = refl
  ; ⇒η = refl
  ; Boolβ₁v = refl
  ; Boolβ₂v = refl
  ; Boolβ₁t = refl
  ; Boolβ₂t = refl
  ; Boolη = λ {Γ A t} → funext λ γ → ind𝟚p
              (λ b → t (π₁ γ ,Σ b) ≡ (if b then t (π₁ γ ,Σ I) else t (π₁ γ ,Σ O)))
              refl refl (π₂ γ)
  ; return[] = refl
  ; to[] = refl
  ; print[] = refl
  ; thunk[] = refl
  ; force[] = refl
  ; lam[] = refl
  ; app[] = refl
  ; true[] = refl
  ; false[] = refl
  ; ite[]v = refl
  ; ite[]t = refl
  }

{-# OPTIONS --prop --rewriting #-}

module FG-CBV+diverge.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import FG-CBV+diverge.Algebra

bind : ∀{i j} {A : Set i} {B : Set j} →
  Maybe A → (A → Maybe B) → Maybe B
bind Nothing _ = Nothing
bind (Just a) f = f a

indMaybe : ∀ {i j} {A : Set i} (P : Maybe A → Prop j) →
  P (Nothing) → ((a : A) → P (Just a)) → (ma : Maybe A) → P ma
indMaybe P n j Nothing = n
indMaybe P n j (Just x) = j x

postulate
  funext : ∀ {i}{A B : Set i}{f g : A → B} →
    ((a : A) → f a ≡ g a) → f ≡ g


model : Algebra
model = record
  { Con = Set
  ; Ty = Set
  ; Sub = λ Γ Δ → Γ → Δ
  ; Val = λ Γ A → Γ → A
  ; Tm = λ Γ A → Γ → Maybe A
  ; _⇒_ = λ A B → A → Maybe B
  ; T = λ A → ↑p 𝟙 → Maybe A
  ; Bool = 𝟚
  ; ∙ = ↑p 𝟙
  ; _▹_ = _×_
  ; id = idf
  ; _∘_ = _∘f_
  ; _,_ = λ δ a γ → δ γ ,Σ a γ
  ; p = π₁
  ; ε = const ↑[ * ]↑
  ; _[_]v = _∘f_
  ; _[_]t = _∘f_
  ; q = π₂
  ; return = λ a γ → Just (a γ)
  ; _to_ = λ ma mb γ → bind (ma γ) λ a → mb (γ ,Σ a)
  ; diverge = const Nothing
  ; thunk = λ ma γ → const (ma γ)
  ; force = λ ma γ → ma γ ↑[ * ]↑
  ; lam = λ mb γ a → mb (γ ,Σ a)
  ; app = λ mb γa → mb (π₁ γa) (π₂ γa)
  ; true = const I
  ; false = const O
  ; itev = λ b u v γ → if b γ then u γ else v γ
  ; itet = λ b u v γ → if b γ then u γ else v γ
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ∙η = refl
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; [id]v = refl
  ; [id]t = refl
  ; [∘]v = refl
  ; [∘]t = refl
  ; toβ = refl
  ; toη = λ {Γ A t} → funext λ γ → indMaybe (λ γ' → bind γ' (λ a → Just a) ≡ γ')
                        refl
                        (λ _ → refl) (t γ)
  ; toass = λ {Γ A B C t u v} → funext λ γ → indMaybe
              (λ γ' → (bind (bind γ' (λ a → u (γ ,Σ a))) (λ a → v (γ ,Σ a)) ≡
                      bind γ' (λ a → bind (u (γ ,Σ a)) (λ a₁ → v (γ ,Σ a₁)))))
                      refl
                      (λ _ → refl)
                      (t γ)
  ; divergeβ = refl
  ; Tβ = refl
  ; Tη = refl
  ; ⇒β = refl
  ; ⇒η = refl
  ; Boolβ₁v = refl
  ; Boolβ₂v = refl
  ; Boolβ₁t = refl
  ; Boolβ₂t = refl
  ; Boolη = λ {Γ A t} → funext λ γ → ind𝟚p
              (λ b → t (π₁ γ ,Σ b) ≡ (if b then t (π₁ γ ,Σ I) else t (π₁ γ ,Σ O)))
              refl refl (π₂ γ)
  ; return[] = refl
  ; to[] = refl
  ; diverge[] = refl
  ; thunk[] = refl
  ; force[] = refl
  ; lam[] = refl
  ; app[] = refl
  ; true[] = refl
  ; false[] = refl
  ; ite[]v = refl
  ; ite[]t = refl
  }

{-# OPTIONS --prop --rewriting #-}

module FG-CBV.Algebra where

open import Lib hiding (_∘_ ; _,_)

module I where
  data Ty : Set where
    _⇒_ : Ty → Ty → Ty
    T : Ty → Ty
    Bool : Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]t
  infixl 6 _[_]v
  infixl 6 _to_
  --infixl 5 _$_
  --infixl 5 _$$_

  postulate
    Val : Con → Ty → Set
    Tm : Con → Ty → Set
    Sub : Con → Con → Set

    id : ∀ {Γ} → Sub Γ Γ
    _∘_ : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Val Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    ε : ∀ {Γ} → Sub Γ ∙

    _[_]v : ∀ {Γ Δ A} → Val Δ A → Sub Γ Δ → Val Γ A
    _[_]t : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    q : ∀ {Γ A} → Val (Γ ▹ A) A

    return : ∀ {Γ A} → Val Γ A → Tm Γ A
    _to_ : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

    thunk : ∀ {Γ A} → Tm Γ A → Val Γ (T A)
    force : ∀ {Γ A} → Val Γ (T A) → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Val Γ (A ⇒ B)
    app : ∀ {Γ A B} → Val Γ (A ⇒ B) → Tm (Γ ▹ A) B

    true : ∀ {Γ} → Val Γ Bool
    false : ∀ {Γ} → Val Γ Bool
    itev : ∀ {Γ A} → Val Γ Bool → Val Γ A → Val Γ A → Val Γ A
    itet : ∀ {Γ A} → Val Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A

    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ass : ∀ {Γ Δ Θ Σ} {ν : Sub Θ Σ} {σ : Sub Δ Θ} {δ : Sub Γ Δ} →
      (ν ∘ σ) ∘ δ ≡ ν ∘ (σ ∘ δ)
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Val Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Val Γ A} → q [ σ , t ]v ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ]v ≡ σ
    [id]v : ∀ {Γ A} {t : Val Γ A} → t [ id ]v ≡ t
    [id]t : ∀ {Γ A} {t : Tm Γ A} → t [ id ]t ≡ t
    [∘]v : ∀ {Γ Δ Θ A} {t : Val Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]v [ δ ]v ≡ t [ σ ∘ δ ]v
    [∘]t : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t

    toβ : ∀ {Γ A B} {t : Val Γ A} {u : Tm (Γ ▹ A) B} →
      return t to u ≡ u [ id , t ]t
    toη : ∀ {Γ A} {t : Tm Γ A} → t to return q ≡ t
    toass : ∀ {Γ A B C} {t : Tm Γ A} {u : Tm (Γ ▹ A) B} {v : Tm (Γ ▹ B) C} →
      t to u to v ≡ t to (u to (v [ p ∘ p , q ]t))

    Tβ : ∀ {Γ A} {t : Tm Γ A} → force (thunk t) ≡ t
    Tη : ∀ {Γ A} {t : Val Γ (T A)} → thunk (force t) ≡ t

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    -- ⇒η : ∀ {Γ A B} {t : Val Γ (A ⇒ B)} → lam (app t) ≡ t -- RIP

    Boolβ₁v : ∀ {Γ A} {u v : Val Γ A} → itev true u v ≡ u
    Boolβ₂v : ∀ {Γ A} {u v : Val Γ A} → itev false u v ≡ v
    Boolβ₁t : ∀ {Γ A} {u v : Tm Γ A} → itet true u v ≡ u
    Boolβ₂t : ∀ {Γ A} {u v : Tm Γ A} → itet false u v ≡ v
    Boolη : ∀ {Γ A} {t : Val (Γ ▹ Bool) A } →
      t ≡ itev q (t [ p , true ]v) (t [ p , false ]v)

    return[] : ∀ {Γ Δ A} {t : Val Δ A} {σ : Sub Γ Δ} →
      (return t) [ σ ]t ≡ return (t [ σ ]v)
    to[] : ∀ {Γ Δ A B} {t : Tm Δ A} {u : Tm (Δ ▹ A) B} {σ : Sub Γ Δ} →
      (t to u) [ σ ]t ≡ (t [ σ ]t) to (u [ σ ∘ p , q ]t)

    thunk[] : ∀ {Γ Δ A} {t : Tm Δ A} {σ : Sub Γ Δ} →
      (thunk t) [ σ ]v ≡ thunk (t [ σ ]t)
    force[] : ∀ {Γ Δ A} {t : Val Δ (T A)} {σ : Sub Γ Δ} →
      (force t) [ σ ]t ≡ force (t [ σ ]v)

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ]v ≡ lam (t [ σ ∘ p , q ]t)
    app[] : ∀ {Γ Δ A B} {t : Val Δ (A ⇒ B)}{σ : Sub Γ Δ} →
      app (t [ σ ]v) ≡ (app t) [ σ ∘ p , q ]t

    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ]v ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ]v ≡ false
    ite[]v : ∀ {Γ Δ A} {b : Val Δ Bool}{u v : Val Δ A}{σ : Sub Γ Δ} →
      (itev b u v) [ σ ]v ≡ itev (b [ σ ]v) (u [ σ ]v) (v [ σ ]v)
    ite[]t : ∀ {Γ Δ A} {b : Val Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (itet b u v) [ σ ]t ≡ itet (b [ σ ]v) (u [ σ ]t) (v [ σ ]t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id]v ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Val Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]v
  ,∘ {δ = δ} = ▹η ⁻¹
            ◾ ap2 _,_ (ass ⁻¹) ([∘]v ⁻¹)
            ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]v) ▹β₂)

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id]v [id]t [∘]v [∘]t #-}
  {-# REWRITE toβ toη toass #-}
  {-# REWRITE ⇒β #-}
  {-# REWRITE Boolβ₁v Boolβ₂v Boolβ₁t Boolβ₂t #-}
  {-# REWRITE lam[] app[] #-}
  {-# REWRITE true[] false[] ite[]v ite[]t #-}

record Algebra {i j k l m} : Set (lsuc (i ⊔ j ⊔ k ⊔ l ⊔ m)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]t
  infixl 6 _[_]v
  infixl 6 _to_

  field
    Con : Set i
    Ty : Set j
    Sub : Con → Con → Set k
    Val : Con → Ty → Set l
    Tm : Con → Ty → Set m

    _⇒_ : Ty → Ty → Ty
    T : Ty → Ty
    Bool : Ty

    ∙ : Con
    _▹_ : Con → Ty → Con
    id : ∀ {Γ} → Sub Γ Γ
    _∘_ : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Val Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    ε : ∀ {Γ} → Sub Γ ∙

    _[_]v : ∀ {Γ Δ A} → Val Δ A → Sub Γ Δ → Val Γ A
    _[_]t : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    q : ∀ {Γ A} → Val (Γ ▹ A) A

    return : ∀ {Γ A} → Val Γ A → Tm Γ A
    _to_ : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

    thunk : ∀ {Γ A} → Tm Γ A → Val Γ (T A)
    force : ∀ {Γ A} → Val Γ (T A) → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Val Γ (A ⇒ B)
    app : ∀ {Γ A B} → Val Γ (A ⇒ B) → Tm (Γ ▹ A) B

    true : ∀ {Γ} → Val Γ Bool
    false : ∀ {Γ} → Val Γ Bool
    itev : ∀ {Γ A} → Val Γ Bool → Val Γ A → Val Γ A → Val Γ A
    itet : ∀ {Γ A} → Val Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A

    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ass : ∀ {Γ Δ Θ Σ} {ν : Sub Θ Σ} {σ : Sub Δ Θ} {δ : Sub Γ Δ} →
      (ν ∘ σ) ∘ δ ≡ ν ∘ (σ ∘ δ)
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Val Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Val Γ A} → q [ σ , t ]v ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ]v ≡ σ
    [id]v : ∀ {Γ A} {t : Val Γ A} → t [ id ]v ≡ t
    [id]t : ∀ {Γ A} {t : Tm Γ A} → t [ id ]t ≡ t
    [∘]v : ∀ {Γ Δ Θ A} {t : Val Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]v [ δ ]v ≡ t [ σ ∘ δ ]v
    [∘]t : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t

    toβ : ∀ {Γ A B} {t : Val Γ A} {u : Tm (Γ ▹ A) B} →
      return t to u ≡ u [ id , t ]t
    toη : ∀ {Γ A} {t : Tm Γ A} → t to return q ≡ t
    toass : ∀ {Γ A B C} {t : Tm Γ A} {u : Tm (Γ ▹ A) B} {v : Tm (Γ ▹ B) C} →
      t to u to v ≡ t to (u to (v [ p ∘ p , q ]t))

    Tβ : ∀ {Γ A} {t : Tm Γ A} → force (thunk t) ≡ t
    Tη : ∀ {Γ A} {t : Val Γ (T A)} → thunk (force t) ≡ t

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    -- ⇒η : ∀ {Γ A B} {t : Val Γ (A ⇒ B)} → lam (app t) ≡ t -- RIP

    Boolβ₁v : ∀ {Γ A} {u v : Val Γ A} → itev true u v ≡ u
    Boolβ₂v : ∀ {Γ A} {u v : Val Γ A} → itev false u v ≡ v
    Boolβ₁t : ∀ {Γ A} {u v : Tm Γ A} → itet true u v ≡ u
    Boolβ₂t : ∀ {Γ A} {u v : Tm Γ A} → itet false u v ≡ v
    Boolη : ∀ {Γ A} {t : Val (Γ ▹ Bool) A } →
      t ≡ itev q (t [ p , true ]v) (t [ p , false ]v)

    return[] : ∀ {Γ Δ A} {t : Val Δ A} {σ : Sub Γ Δ} →
      (return t) [ σ ]t ≡ return (t [ σ ]v)
    to[] : ∀ {Γ Δ A B} {t : Tm Δ A} {u : Tm (Δ ▹ A) B} {σ : Sub Γ Δ} →
      (t to u) [ σ ]t ≡ (t [ σ ]t) to (u [ σ ∘ p , q ]t)

    thunk[] : ∀ {Γ Δ A} {t : Tm Δ A} {σ : Sub Γ Δ} →
      (thunk t) [ σ ]v ≡ thunk (t [ σ ]t)
    force[] : ∀ {Γ Δ A} {t : Val Δ (T A)} {σ : Sub Γ Δ} →
      (force t) [ σ ]t ≡ force (t [ σ ]v)

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ]v ≡ lam (t [ σ ∘ p , q ]t)
    app[] : ∀ {Γ Δ A B} {t : Val Δ (A ⇒ B)}{σ : Sub Γ Δ} →
      app (t [ σ ]v) ≡ (app t) [ σ ∘ p , q ]t

    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ]v ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ]v ≡ false
    ite[]v : ∀ {Γ Δ A} {b : Val Δ Bool}{u v : Val Δ A}{σ : Sub Γ Δ} →
      (itev b u v) [ σ ]v ≡ itev (b [ σ ]v) (u [ σ ]v) (v [ σ ]v)
    ite[]t : ∀ {Γ Δ A} {b : Val Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (itet b u v) [ σ ]t ≡ itet (b [ σ ]v) (u [ σ ]t) (v [ σ ]t)

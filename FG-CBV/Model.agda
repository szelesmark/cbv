{-# OPTIONS --prop --rewriting #-}

module FG-CBV.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import FG-CBV.Algebra

open I

postulate
  funext : ∀ {i}{A B : Set i}{f g : A → B} →
    ((a : A) → f a ≡ g a) → f ≡ g

model : Algebra
model = record
  { Con = Set
    ; Ty = Set
    ; Sub = λ Γ Δ → Γ → Δ
    ; To = λ Γ Δ → Γ → Δ
    ; Val = λ Γ A → Γ → A
    ; Ret = λ Γ A → Γ → A
    ; ∙ = ↑p 𝟙
    ; _▹_ = _×_
    ; _⇒_ = λ A B → A → B
    ; Bool = 𝟚
    ; _∘_ = _∘f_
    ; id = idf
    ; ε = const *↑
    ; _∘ₜ_ = _∘f_
    ; idₜ = idf
    ; εₜ = const *↑
    ; _,_ = λ σ t γ → σ γ ,Σ t γ
    ; p = π₁
    ; _,ₜ_ = λ σ t γ → σ γ ,Σ t γ
    ; pₜ = π₁
    ; q = π₂
    ; _[_]r = _∘f_
    ; _[_]v = _∘f_
    ; _⟨_⟩ = _∘f_
    ; return = idf
    ; lam = λ t γ x → t (γ ,Σ x)
    ; app = λ t γx → t (π₁ γx) (π₂ γx)
    ; true = const I
    ; false = const O
    ; ite = λ b u v γ → if b γ then u γ else v γ
    ; ass = refl
    ; idl = refl
    ; idr = refl
    ; ∙η = refl
    ; ▹β₁ = refl
    ; ▹β₂ = refl
    ; ▹η = refl
    ; [id]r = refl
    ; [id]v = refl
    ; [∘]r = refl
    ; [∘]v = refl
    ; assₜ = refl
    ; idlₜ = refl
    ; idrₜ = refl
    ; ∙ηₜ = refl
    ; ⇒β = refl
    ; ⇒η = refl
    ; Boolβ₁ = refl
    ; Boolβ₂ = refl
    ; Toβ₁ = refl
    ; Toβ₂ = refl
    ; Toη = refl
    ; ⟨id⟩ = refl
    ; ⟨∘⟩ = refl
    ; lam[] = refl
    ; return[] = refl
    ; true[] = refl
    ; false[] = refl
    ; ite[] = refl
    ; stuff = refl
  }

{-# OPTIONS --prop --rewriting #-}

module FG-CBV.CPO-Model where

open import FG-CBV.Algebra
open import Lib

record dCPO {i j} : Set (lsuc (i ⊔ j)) where
  field
    X : Set i
    _⊑_ : X → X → Prop j
    refl⊑ : ∀{x} → x ⊑ x
    trans⊑ : ∀{x y z} → x ⊑ y → y ⊑ z → x ⊑ z
    antisym⊑ : ∀{x y} → x ⊑ y → y ⊑ x → x ≡ y
    ⊔ : (_∈S : X → Prop) →
        ((x y : X) → x ∈S ×p y ∈S → Σ X (λ z → ↑p (z ∈S ×p x ⊑ z ×p y ⊑ z))) →
        X
    ιn : ∀ {_∈S p d} → ((x : X) → x ∈S → x ⊑ d) → ⊔ _∈S p ⊑ d
    out : ∀ {_∈S p d} → ⊔ _∈S p ⊑ d → (x : X) → x ∈S → x ⊑ d

-- Scott-continuous functions 
record ContFun {i j} (X Y : dCPO {i}{j}) : Set (lsuc (i ⊔ j)) where
  private
    module X = dCPO X
    module Y = dCPO Y
  field
    f : X.X → Y.X
    directed : ∀ {_∈S : X.X → Prop} →
               (∀{x y} → x ∈S → y ∈S →  Σ X.X (λ z → ↑p (z ∈S ×p x X.⊑ z ×p y X.⊑ z))) →
               Σ (Y.X → Prop) λ _∈S' → {!(∀{x} → x ∈S → f x ∈S' !}

cpo-model : Algebra
cpo-model = record
              { Con = {!!}
              ; Ty = {!!}
              ; Sub = {!!}
              ; Val = {!!}
              ; Tm = {!!}
              ; _⇒_ = {!!}
              ; T = {!!}
              ; Bool = {!!}
              ; ∙ = {!!}
              ; _▹_ = {!!}
              ; id = {!!}
              ; _∘_ = {!!}
              ; _,_ = {!!}
              ; p = {!!}
              ; ε = {!!}
              ; _[_]v = {!!}
              ; _[_]t = {!!}
              ; q = {!!}
              ; return = {!!}
              ; _to_ = {!!}
              ; thunk = {!!}
              ; force = {!!}
              ; lam = {!!}
              ; app = {!!}
              ; true = {!!}
              ; false = {!!}
              ; itev = {!!}
              ; itet = {!!}
              ; idl = {!!}
              ; idr = {!!}
              ; ass = {!!}
              ; ∙η = {!!}
              ; ▹β₁ = {!!}
              ; ▹β₂ = {!!}
              ; ▹η = {!!}
              ; [id]v = {!!}
              ; [id]t = {!!}
              ; [∘]v = {!!}
              ; [∘]t = {!!}
              ; toβ = {!!}
              ; toη = {!!}
              ; toass = {!!}
              ; Tβ = {!!}
              ; Tη = {!!}
              ; ⇒β = {!!}
              ; Boolβ₁v = {!!}
              ; Boolβ₂v = {!!}
              ; Boolβ₁t = {!!}
              ; Boolβ₂t = {!!}
              ; Boolη = {!!}
              ; return[] = {!!}
              ; to[] = {!!}
              ; thunk[] = {!!}
              ; force[] = {!!}
              ; lam[] = {!!}
              ; app[] = {!!}
              ; true[] = {!!}
              ; false[] = {!!}
              ; ite[]v = {!!}
              ; ite[]t = {!!}
              }

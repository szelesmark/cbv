{-# OPTIONS --prop --rewriting #-}

module FG-CBV+fix.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import FG-CBV+fix.Algebra

-- postulate
  -- funext : ∀ {i}{A B : Set i}{f g : A → B} →
    -- ((a : A) → f a ≡ g a) → f ≡ g

≤M : ∀{A : Set} → (A → A → Prop) → Maybe A → Maybe A → Prop
≤M ⊑ Nothing ma = 𝟙
≤M ⊑ (Just x) Nothing = 𝟘
≤M ⊑ (Just x) (Just y) = ⊑ x y 

record ωCPO (A : Set) : Set₁ where
  infix 5 _⊑_

  field
    D : Set
    _⊑_ : D → D → Prop

    ⊔ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D
    η : A → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ⊔ f p ⊑ d
    out : ∀ {f p d} → ⊔ f p ⊑ d → (n : ℕ) → f n ⊑ d
open ωCPO

record ωCPOMap {X}{Y}(A : ωCPO X)(B : ωCPO Y) : Set where
  module A = ωCPO A
  module B = ωCPO B
  field
    m : A.D → B.D
    _⊑_ : ∀ {d d'} → d A.⊑ d' → m d B.⊑ m d'
    ⊔ : ∀ {f p} → m (A.⊔ f p) ≡ B.⊔ (m ∘f f) λ n → _⊑_ (p n)  

record ωCPO⊥ (A : Set) : Set₁ where
  infix 5 _⊑_

  field
    D : Set
    _⊑_ : D → D → Prop

    ⊔ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D
    η : A → D
    ⊥ : D
    
    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d
    
    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ⊔ f p ⊑ d
    out : ∀ {f p d} → ⊔ f p ⊑ d → (n : ℕ) → f n ⊑ d


record ωCPOMap⊥ {X}{Y}(A : ωCPO X)(B : ωCPO⊥ Y) : Set where
  module A = ωCPO A
  module B = ωCPO⊥ B
  field
    m : A.D → B.D
    _⊑_ : ∀ {d d'} → d A.⊑ d' → m d B.⊑ m d'
    ⊔ : ∀ {f p} → m (A.⊔ f p) ≡ B.⊔ (m ∘f f) λ n → _⊑_ (p n)

record Stream' {X}(A : ωCPO X) : Set₁ where
  coinductive
  field
    pred : Stream' A ⊎ D A
open Stream'

eps : ∀ {X}{A : ωCPO X} → Stream' A → Stream' A
pred (eps s) = ι₁ s

val : ∀ {X}{A : ωCPO X} → D A → Stream' A
pred (val d) = ι₂ d

pred-nth : ∀ {X}{A : ωCPO X} → ℕ → Stream' A → Stream' A
pred-nth O s = s
pred-nth (S n) s = case⊎ (pred s) idf val

record _≤str_ {X}{A : ωCPO X} (x y : Stream' A) : Prop₁ where
  coinductive
  module A = ωCPO A
  field
    ≤epseps : eps x ≤str eps y
    ≤epsval : ∀ {d} → x ≤str val d → eps x ≤str val d
    ≤val : ∀ {d d' n} → pred-nth n y ≡ val d' → d A.⊑ d' → val d ≤str y 

model : Algebra
model = record
          { Con = Σ Set ωCPO
          ; Ty = Σ Set ωCPO
          ; Sub = λ Γ Δ → ωCPOMap (π₂ Γ) (π₂ Δ)
          ; Val = λ Γ A → ωCPOMap (π₂ Γ) (π₂ A)
          ; Tm = λ Γ A → {!ωCPOMap !}
          ; _⇒_ = {!!}
          ; T = {!!}
          ; Bool = {!!}
          ; ∙ = {!!}
          ; _▹_ = {!!}
          ; id = {!!}
          ; _∘_ = {!!}
          ; _,_ = {!!}
          ; p = {!!}
          ; ε = {!!}
          ; _[_]v = {!!}
          ; _[_]t = {!!}
          ; q = {!!}
          ; return = {!!}
          ; _to_ = {!!}
          ; fix = {!!}
          ; thunk = {!!}
          ; force = {!!}
          ; lam = {!!}
          ; app = {!!}
          ; true = {!!}
          ; false = {!!}
          ; itev = {!!}
          ; itet = {!!}
          ; idl = {!!}
          ; idr = {!!}
          ; ass = {!!}
          ; ∙η = {!!}
          ; ▹β₁ = {!!}
          ; ▹β₂ = {!!}
          ; ▹η = {!!}
          ; [id]v = {!!}
          ; [id]t = {!!}
          ; [∘]v = {!!}
          ; [∘]t = {!!}
          ; toβ = {!!}
          ; toη = {!!}
          ; toass = {!!}
          ; fixβ = {!!}
          ; Tβ = {!!}
          ; Tη = {!!}
          ; ⇒β = {!!}
          ; Boolβ₁v = {!!}
          ; Boolβ₂v = {!!}
          ; Boolβ₁t = {!!}
          ; Boolβ₂t = {!!}
          ; Boolη = {!!}
          ; return[] = {!!}
          ; to[] = {!!}
          ; fix[] = {!!}
          ; thunk[] = {!!}
          ; force[] = {!!}
          ; lam[] = {!!}
          ; app[] = {!!}
          ; true[] = {!!}
          ; false[] = {!!}
          ; ite[]v = {!!}
          ; ite[]t = {!!}
          }

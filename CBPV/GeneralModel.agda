{-# OPTIONS --prop --rewriting #-}

module CBPV.GeneralModel where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import CBPV.Algebra

postulate
  funext : ∀ {i}{A B : Set i}{f g : A → B} →
    ((a : A) → f a ≡ g a) → f ≡ g

α : ∀ {A B C : Set} → (A × B) × C → A × (B × C)
α (a ,Σ b ,Σ c) = a ,Σ (b ,Σ c)

-- strong monad on the category Set
record StrongMonad : Set₁ where
  field
    -- functor structure
    T : Set → Set
    T₁ : ∀ {A B} → (A → B) → T A → T B
    mapid : ∀ {A} {ta : T A} → T₁ idf ta ≡ ta
    map∘ : ∀ {A B C} {f : A → B} {g : B → C} {ta : T A} →
      T₁ (g ∘f f) ta ≡ T₁ g (T₁ f ta)

    -- monad structure
    η : ∀ {A} → A → T A
    μ : ∀ {A} → T (T A) → T A
    ηnat : ∀ {A B} {f : A → B} {x : A} → T₁ f (η x) ≡ η (f x)
    μnat : ∀ {A B} {f : A → B} {x : T (T A)} → T₁ f (μ x) ≡ μ (T₁ (T₁ f) x)
    μass : ∀ {A} {ttta : T (T (T A))} → μ ((T₁ μ) ttta) ≡ μ (μ ttta)
    μidr : ∀ {A} {ta : T A} → μ (T₁ η ta) ≡ ta
    μidl : ∀ {A} {ta : T A} → μ (η ta) ≡ ta

    -- tensorial strength
    t : ∀ {A B} → A × T B → T (A × B)
    tid : ∀ {A} {1ta : ↑p 𝟙 × T A} →  T₁ π₂ (t 1ta) ≡ π₂ 1ta
    t× : ∀ {A B C} {x : (A × B) × T C} →
       t (π₁ (α x) ,Σ t (π₂ (α x))) ≡ T₁ α (t x)
    tη : ∀ {A B} {x : A × B} → t (π₁ x ,Σ η (π₂ x)) ≡ η x
    tμ : ∀ {A B} {x : A × T (T B)} →
      μ (T₁ t (t x)) ≡ t (π₁ x ,Σ μ (π₂ x))

open StrongMonad

record T-algebra (T : StrongMonad) (X : Set) : Set₁ where
  field
    θ : let module T = StrongMonad T in T.T X → X
    ηθ : ∀ {x} → θ (η T x) ≡ x
    μθ : ∀ {ttx} → θ (μ T ttx) ≡ θ (T₁ T θ ttx)
open T-algebra

generalModel : StrongMonad → Algebra
generalModel T = let module T = StrongMonad T in record
  { Con = Set
  ; Sub = λ Γ Δ → Γ → Δ
  ; TyV = Set
  ; TyC = Σ Set (T-algebra T)
  ; Val = λ Γ A → Γ → A
  ; Tm = λ Γ A → Γ → π₁ A
  ; U = π₁
  ; Bool = 𝟚
  ; _×_ = _×_
  ; F = λ A → (T.T A) ,Σ record { θ = T.μ
                                ; ηθ = T.μidl
                                ; μθ = T.μass ⁻¹
                                }
  ; _π_ = λ A B → (π₁ A × π₁ B) ,Σ let module A = T-algebra (π₂ A)
                                       module B = T-algebra (π₂ B)
                                   in record
                                   { θ = λ tab → (A.θ (T.T₁ π₁ tab)) ,Σ (B.θ (T.T₁ π₂ tab))
                                   ; ηθ = ap2 _,Σ_
                                              (ap A.θ T.ηnat ◾ A.ηθ)
                                              (ap B.θ T.ηnat ◾ B.ηθ)
                                   ; μθ = ap2 _,Σ_
                                              (ap A.θ (ap (T.T₁ π₁) {!!}))
                                              {!!} }
  ; _⇒_ = λ A B → (A → π₁ B) ,Σ let module B = T-algebra (π₂ B) in
                              record
                                { θ = λ t a → B.θ (T.T₁ (λ f → f a) t)
                                ; ηθ = funext λ a → ap B.θ T.ηnat ◾ B.ηθ  
                                ; μθ = funext λ a → ap B.θ T.μnat
                                                    ◾ B.μθ
                                                    ◾ ap B.θ {!!} }
  ; ∙ = ↑p 𝟙
  ; _▹_ = _×_
  ; id = idf
  ; _∘_ = _∘f_
  ; _,_ = λ σ a γ → σ γ ,Σ a γ
  ; p = π₁
  ; ε = const *↑
  ; _[_]v = _∘f_
  ; _[_]t = _∘f_
  ; q = π₂
  ; return = λ a γ → T.η (a γ)
  ; _to_ = λ {_ _ B} ta u γ → let module B = T-algebra (π₂ B)
                              in B.θ (T.T₁ u (T.t (γ ,Σ ta γ)))
  ; thunk = idf
  ; force = idf
  ; lam = λ t γ a → t (γ ,Σ a)
  ; app = λ t γa → t (π₁ γa) (π₂ γa)
  ; true = const I
  ; false = const O
  ; itev = λ b u v γ → if b γ then u γ else v γ
  ; itet = λ b u v γ → if b γ then u γ else v γ
  ; _,×_ = λ a b γ → a γ ,Σ b γ
  ; pm×v = λ ab t γ → t (γ ,Σ π₁ (ab γ) ,Σ π₂ (ab γ))
  ; pm×t = λ ab t γ → t (γ ,Σ π₁ (ab γ) ,Σ π₂ (ab γ))
  ; _,π_ = λ a b γ → a γ ,Σ b γ
  ; proj₁ = λ ab γ → π₁ (ab γ)
  ; proj₂ = λ ab γ → π₂ (ab γ)
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ∙η = refl
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; [id]v = refl
  ; [id]t = refl
  ; [∘]v = refl
  ; [∘]t = refl
  ; toβ = λ {Γ A B u v} → let module B = T-algebra (π₂ B) in funext λ γ →
                          ap (λ x → (B.θ (T.T₁ v x))) T.tη
                          ◾ ap B.θ T.ηnat
                          ◾ B.ηθ
<<<<<<< HEAD
  ; toη = λ {Γ A u} → funext λ γ → ap T.μ {!!} ◾ {!!} 
  ; toass = λ {Γ A B C t u v} → funext λ γ → let module C = T-algebra (π₂ C)
                                             in ap (C.θ ∘f T.T₁ v) (T.tμ ⁻¹)
                                                ◾ ap C.θ T.μnat
                                                ◾ {!T.tμ!}
                                                
  ; toπ = funext λ γ → {!!}   
=======
  ; toη = λ {Γ A u} → funext λ γ → {!  !}
  ; toass = λ {_ _ _ C} → funext λ γ → {!!} 
  ; toπ = {!   !}
>>>>>>> 7bba0748c1c77a70b031c906cdac7be27dc6cb25
  ; to⇒ = {!   !}
  ; Uβ = refl
  ; Uη = refl
  ; ⇒β = refl
  ; ⇒η = refl
  ; Boolβ₁v = refl
  ; Boolβ₂v = refl
  ; Boolβ₁t = refl
  ; Boolβ₂t = refl
  ; Boolη = λ {Γ A t} → funext λ γ → ind𝟚p
              (λ b → t (π₁ γ ,Σ b) ≡ (if b then t (π₁ γ ,Σ I) else t (π₁ γ ,Σ O)))
              refl refl (π₂ γ)
  ; ×βv = refl
  ; ×βt = refl
  ; ×ηv = refl
  ; ×ηt = refl
  ; πβ₁ = refl
  ; πβ₂ = refl
  ; πη = refl
  ; return[] = refl
  ; to[] = {!   !}
  ; thunk[] = refl
  ; force[] = refl
  ; lam[] = refl
  ; app[] = refl
  ; true[] = refl
  ; false[] = refl
  ; ite[]v = refl
  ; ite[]t = refl
  ; ,×[] = refl
  ; pm×v[] = refl
  ; pm×t[] = refl
  ; ,π[] = refl
  ; proj₁[] = refl
  ; proj₂[] = refl
  }

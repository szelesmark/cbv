{-# OPTIONS --prop --rewriting #-}

module CBPV.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import CBPV.Algebra

postulate
  funext : ∀ {i}{A B : Set i}{f g : A → B} →
    ((a : A) → f a ≡ g a) → f ≡ g

Model : Algebra
Model = record
  { Con = Set
  ; Sub = λ Γ Δ → Γ → Δ
  ; TyV = Set
  ; TyC = Set
  ; Val = λ Γ A → Γ → A
  ; Tm = λ Γ A → Γ → A
  ; U = idf
  ; Bool = 𝟚
  ; _×_ = _×_
  ; F = idf
  ; _π_ = _×_
  ; _⇒_ = λ A B → A → B
  ; ∙ = ↑p 𝟙
  ; _▹_ = _×_
  ; id = idf
  ; _∘_ = _∘f_
  ; _,_ = λ σ a γ → σ γ ,Σ a γ
  ; p = π₁
  ; ε = const *↑
  ; _[_]v = _∘f_
  ; _[_]t = _∘f_
  ; q = π₂
  ; return = idf
  ; _to_ = λ a t γ → t (γ ,Σ a γ)
  ; thunk = idf
  ; force = idf
  ; lam = λ t γ a → t (γ ,Σ a)
  ; app = λ t γa → t (π₁ γa) (π₂ γa)
  ; true = const I
  ; false = const O
  ; itev = λ b u v γ → if b γ then u γ else v γ
  ; itet = λ b u v γ → if b γ then u γ else v γ
  ; _,×_ = λ a b γ → a γ ,Σ b γ
  ; pm×v = λ ab t γ → t (γ ,Σ π₁ (ab γ) ,Σ π₂ (ab γ))
  ; pm×t = λ ab t γ → t (γ ,Σ π₁ (ab γ) ,Σ π₂ (ab γ))
  ; _,π_ = λ a b γ → a γ ,Σ b γ
  ; proj₁ = λ ab γ → π₁ (ab γ)
  ; proj₂ = λ ab γ → π₂ (ab γ)
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ∙η = refl
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; [id]v = refl
  ; [id]t = refl
  ; [∘]v = refl
  ; [∘]t = refl
  ; toβ = refl
  ; toη = refl
  ; toass = refl
  ; toπ = refl
  ; to⇒ = refl
  ; Uβ = refl
  ; Uη = refl
  ; ⇒β = refl
  ; ⇒η = refl
  ; Boolβ₁v = refl
  ; Boolβ₂v = refl
  ; Boolβ₁t = refl
  ; Boolβ₂t = refl
  ; Boolη = λ {Γ A t} → funext λ γ → ind𝟚p
              (λ b → t (π₁ γ ,Σ b) ≡ (if b then t (π₁ γ ,Σ I) else t (π₁ γ ,Σ O)))
              refl refl (π₂ γ)
  ; ×βv = refl
  ; ×βt = refl
  ; ×ηv = refl
  ; ×ηt = refl
  ; πβ₁ = refl
  ; πβ₂ = refl
  ; πη = refl
  ; return[] = refl
  ; to[] = refl
  ; thunk[] = refl
  ; force[] = refl
  ; lam[] = refl
  ; app[] = refl
  ; true[] = refl
  ; false[] = refl
  ; ite[]v = refl
  ; ite[]t = refl
  ; ,×[] = refl
  ; pm×v[] = refl
  ; pm×t[] = refl
  ; ,π[] = refl
  ; proj₁[] = refl
  ; proj₂[] = refl
  }

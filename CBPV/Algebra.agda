{-# OPTIONS --prop --rewriting #-}

module CBPV.Algebra where

open import Lib hiding (_∘_ ; _,_ ; _×_)

record Algebra {i j k l m n} : Set (lsuc (i ⊔ j ⊔ k ⊔ l ⊔ m ⊔ n)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 5 _×_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]t
  infixl 6 _[_]v
  infixl 6 _to_
  infixl 7 _,×_
  infixl 7 _,π_

  field
    Con : Set i
    Sub : Con → Con → Set j
    TyV : Set k
    TyC : Set l
    Val : Con → TyV → Set m
    Tm : Con → TyC → Set n

    U : TyC → TyV
    Bool : TyV
    _×_ : TyV → TyV → TyV

    F : TyV → TyC
    _π_ : TyC → TyC → TyC
    _⇒_ : TyV → TyC → TyC

    ∙ : Con
    _▹_ : Con → TyV → Con
    id : ∀ {Γ} → Sub Γ Γ
    _∘_ : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Val Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    ε : ∀ {Γ} → Sub Γ ∙

    _[_]v : ∀ {Γ Δ A} → Val Δ A → Sub Γ Δ → Val Γ A
    _[_]t : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    q : ∀ {Γ A} → Val (Γ ▹ A) A

    return : ∀ {Γ A} → Val Γ A → Tm Γ (F A)
    _to_ : ∀ {Γ A B} → Tm Γ (F A) → Tm (Γ ▹ A) B → Tm Γ B

    thunk : ∀ {Γ A} → Tm Γ A → Val Γ (U A)
    force : ∀ {Γ A} → Val Γ (U A) → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B

    true : ∀ {Γ} → Val Γ Bool
    false : ∀ {Γ} → Val Γ Bool
    itev : ∀ {Γ A} → Val Γ Bool → Val Γ A → Val Γ A → Val Γ A
    itet : ∀ {Γ A} → Val Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A

    _,×_ : ∀ {Γ A B} → Val Γ A → Val Γ B → Val Γ (A × B)
    pm×v : ∀ {Γ A B C} → Val Γ (A × B) → Val (Γ ▹ A ▹ B) C → Val Γ C
    pm×t : ∀ {Γ A B C} → Val Γ (A × B) → Tm (Γ ▹ A ▹ B) C → Tm Γ C

    _,π_ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A π B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A π B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A π B) → Tm Γ B

    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ass : ∀ {Γ Δ Θ Σ} {ν : Sub Θ Σ} {σ : Sub Δ Θ} {δ : Sub Γ Δ} →
      (ν ∘ σ) ∘ δ ≡ ν ∘ (σ ∘ δ)
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Val Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Val Γ A} → q [ σ , t ]v ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ]v ≡ σ
    [id]v : ∀ {Γ A} {t : Val Γ A} → t [ id ]v ≡ t
    [id]t : ∀ {Γ A} {t : Tm Γ A} → t [ id ]t ≡ t
    [∘]v : ∀ {Γ Δ Θ A} {t : Val Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]v [ δ ]v ≡ t [ σ ∘ δ ]v
    [∘]t : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t

    toβ : ∀ {Γ A B} {t : Val Γ A} {u : Tm (Γ ▹ A) B} →
      return t to u ≡ u [ id , t ]t
    toη : ∀ {Γ A} {t : Tm Γ (F A)} → t to return q ≡ t
    toass : ∀ {Γ A B C} {t : Tm Γ (F A)} {u : Tm (Γ ▹ A) (F B)} {v : Tm (Γ ▹ B) C} →
        t to u to v ≡ t to (u to (v [ p ∘ p , q ]t))
    toπ : ∀ {Γ A B C} {t : Tm Γ (F A)} {u : Tm (Γ ▹ A) B} {v : Tm (Γ ▹ A) C} →
      t to (u ,π v) ≡ ((t to u) ,π (t to v))
    to⇒ : ∀ {Γ A B C} {t : Tm Γ (F A)} {u : Tm (Γ ▹ A) (B ⇒ C)} →
      t to u ≡ lam (t [ p ]t to (app u [ p ∘ p , q , q [ p ]v ]t))

    Uβ : ∀ {Γ A} {t : Tm Γ A} → force (thunk t) ≡ t
    Uη : ∀ {Γ A} {t : Val Γ (U A)} → thunk (force t) ≡ t

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    Boolβ₁v : ∀ {Γ A} {u v : Val Γ A} → itev true u v ≡ u
    Boolβ₂v : ∀ {Γ A} {u v : Val Γ A} → itev false u v ≡ v
    Boolβ₁t : ∀ {Γ A} {u v : Tm Γ A} → itet true u v ≡ u
    Boolβ₂t : ∀ {Γ A} {u v : Tm Γ A} → itet false u v ≡ v
    Boolη : ∀ {Γ A} {t : Val (Γ ▹ Bool) A } →
      t ≡ itev q (t [ p , true ]v) (t [ p , false ]v)

    ×βv : ∀ {Γ A B C} {t : Val Γ A} {u : Val Γ B} {v : Val (Γ ▹ A ▹ B) C} →
      pm×v (t ,× u) v ≡ v [ id , t , u ]v
    ×βt : ∀ {Γ A B C} {t : Val Γ A} {u : Val Γ B} {v : Tm (Γ ▹ A ▹ B) C} →
        pm×t (t ,× u) v ≡ v [ id , t , u ]t
    ×ηv : ∀ {Γ A B C} {t : Val (Γ ▹ (A × B)) C} {u : Val Γ (A × B)} →
      t [ id , u ]v ≡ pm×v u (t [ p ∘ p , (q [ p ]v) ,× q ]v)
    ×ηt : ∀ {Γ A B C} {t : Tm (Γ ▹ (A × B)) C} {u : Val Γ (A × B)} →
      t [ id , u ]t ≡ pm×t u (t [ p ∘ p , (q [ p ]v) ,× q ]t)

    πβ₁ : ∀ {Γ A B} {t : Tm Γ A} {u : Tm Γ B} → proj₁ (t ,π u) ≡ t
    πβ₂ : ∀ {Γ A B} {t : Tm Γ A} {u : Tm Γ B} → proj₂ (t ,π u) ≡ u
    πη : ∀ {Γ A B} {t : Tm Γ (A π B)} → t ≡ proj₁ t ,π proj₂ t

    return[] : ∀ {Γ Δ A} {t : Val Δ A} {σ : Sub Γ Δ} →
      (return t) [ σ ]t ≡ return (t [ σ ]v)
    to[] : ∀ {Γ Δ A B} {t : Tm Δ (F A)} {u : Tm (Δ ▹ A) B} {σ : Sub Γ Δ} →
      (t to u) [ σ ]t ≡ (t [ σ ]t) to (u [ σ ∘ p , q ]t)

    thunk[] : ∀ {Γ Δ A} {t : Tm Δ A} {σ : Sub Γ Δ} →
      (thunk t) [ σ ]v ≡ thunk (t [ σ ]t)
    force[] : ∀ {Γ Δ A} {t : Val Δ (U A)} {σ : Sub Γ Δ} →
      (force t) [ σ ]t ≡ force (t [ σ ]v)

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ]t ≡ lam (t [ σ ∘ p , q ]t)
    app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
      app (t [ σ ]t) ≡ (app t) [ σ ∘ p , q ]t

    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ]v ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ]v ≡ false
    ite[]v : ∀ {Γ Δ A} {b : Val Δ Bool}{u v : Val Δ A}{σ : Sub Γ Δ} →
      (itev b u v) [ σ ]v ≡ itev (b [ σ ]v) (u [ σ ]v) (v [ σ ]v)
    ite[]t : ∀ {Γ Δ A} {b : Val Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (itet b u v) [ σ ]t ≡ itet (b [ σ ]v) (u [ σ ]t) (v [ σ ]t)

    ,×[] : ∀ {Γ Δ A B} {t : Val Δ A} {u : Val Δ B} {σ : Sub Γ Δ} →
      (t ,× u) [ σ ]v ≡ (t [ σ ]v) ,× (u [ σ ]v)
    pm×v[] : ∀ {Γ Δ A B C} {t : Val Δ (A × B)} {u : Val (Δ ▹ A ▹ B) C}
      {σ : Sub Γ Δ} →
      (pm×v t u) [ σ ]v ≡ pm×v (t [ σ ]v) (u [ σ ∘ p ∘ p , q [ p ]v , q ]v)
    pm×t[] : ∀ {Γ Δ A B C} {t : Val Δ (A × B)} {u : Tm (Δ ▹ A ▹ B) C}
      {σ : Sub Γ Δ} →
      (pm×t t u) [ σ ]t ≡ pm×t (t [ σ ]v) (u [ σ ∘ p ∘ p , q [ p ]v , q ]t)

    ,π[] : ∀ {Γ Δ A B} {t : Tm Δ A} {u : Tm Δ B} {σ : Sub Γ Δ} →
      (t ,π u) [ σ ]t ≡ (t [ σ ]t) ,π (u [ σ ]t)
    proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A π B)} {σ : Sub Γ Δ} →
      (proj₁ t) [ σ ]t ≡ proj₁ (t [ σ ]t )
    proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A π B)} {σ : Sub Γ Δ} →
      (proj₂ t) [ σ ]t ≡ proj₂ (t [ σ ]t )

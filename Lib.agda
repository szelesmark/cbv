{-# OPTIONS --prop --rewriting #-}

module Lib where

open import Agda.Primitive public

idf : ∀ {i} {A : Set i} → A → A
idf x = x

const : ∀ {i j} {A : Set i}{B : Set j} → A → B → A
const x y = x

infixl 6 _⊙_
_⊙_ : ∀ {i j k} {A : Set i}{B : A → Set j}{C : ∀ {x} → B x → Set k}
  (f : ∀ {x} (y : B x) → C y)(g : (x : A) → B x)
  (x : A) → C (g x)
(f ⊙ g) x = f (g x)

infixl 6 _∘_
_∘_ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} →
  (B → C) → (A → B) → A → C
(f ∘ g) x = f (g x)

infixl 6 _∘'_
_∘'_ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} →
  (A → B → C) → (A → B) → A → C
(f ∘' g) x = f x (g x)

data 𝟘 : Prop where

⟦_⟧𝟘 : ∀ {i} {A : Set i} → 𝟘 → A
⟦ () ⟧𝟘

⟦_⟧𝟘p : ∀ {i} {A : Prop i} → 𝟘 → A
⟦ () ⟧𝟘p

record 𝟙 : Prop where
  constructor *
open 𝟙 public

record 𝟙' {i} : Prop i where
  constructor *'
open 𝟙' public

data 𝟚 : Set where
  O : 𝟚
  I : 𝟚

ind𝟚 : ∀ {i} (P : 𝟚 → Set i) →
  P O → P I →
  (b : 𝟚) → P b
ind𝟚 P u v O = u
ind𝟚 P u v I = v

ind𝟚p : ∀ {i} (P : 𝟚 → Prop i) →
  P O → P I →
  (b : 𝟚) → P b
ind𝟚p P u v O = u
ind𝟚p P u v I = v

if_then_else_ : ∀ {i} {A : Set i} → 𝟚 → A → A → A
if b then u else v = ind𝟚 _ v u b

data ℕ : Set where
  O : ℕ
  S : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

indℕ : ∀ {i} (P : ℕ → Set i) →
  P O → ({n : ℕ} → P n → P (S n)) →
  (n : ℕ) → P n
indℕ P u v O = u
indℕ P u v (S n) = v (indℕ P u v n)

indℕp : ∀ {i} (P : ℕ → Prop i) →
  P O → ({n : ℕ} → P n → P (S n)) →
  (n : ℕ) → P n
indℕp P u v O = u
indℕp P u v (S n) = v (indℕp P u v n)


max : ℕ → ℕ → ℕ
max O     b = b
max (S a) O = S a
max (S a) (S b) = S (max a b)

infixl 7 _+ℕ_
_+ℕ_ : ℕ → ℕ → ℕ
O +ℕ b = b
S a +ℕ b = S (a +ℕ b)

infix 4 _≤_
_≤_ : ℕ → ℕ → Prop
O ≤ b = 𝟙
S a ≤ O = 𝟘
S a ≤ S b = a ≤ b

infixl 8 _*ℕ_
_*ℕ_ : ℕ → ℕ → ℕ
O *ℕ b = 0
S a *ℕ b = b +ℕ a *ℕ b

infix 9 _^ℕ_
_^ℕ_ : ℕ → ℕ → ℕ
a ^ℕ O = 1
a ^ℕ S b = a *ℕ a ^ℕ b

infixr 5 _::_
data Vec {i} (A : Set i) : ℕ → Set i where
  [] : Vec A O
  _::_ : ∀ {n} → A → Vec A n → Vec A (S n)

infixr 5 _∷_
data List {i} (A : Set i) : Set i where
  [] : List A
  _∷_ : A → List A → List A

infixl 7 _++_
_++_ : ∀ {i}{A : Set i} → List A → List A → List A
[]       ++ ys = ys
(x ∷ xs) ++ ys = x ∷ (xs ++ ys) 

infixl 4 _,_
record Σ {i j} (A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    π₁ : A
    π₂ : B π₁
open Σ public

infixl 4 _,p_
record Σp {i j} (A : Prop i) (B : A → Prop j) : Prop (i ⊔ j) where
  constructor _,p_
  field
    π₁ : A
    π₂ : B π₁
open Σp public

infixr 1 _⊎_
data _⊎_ {i j} (A : Set i)(B : Set j) : Set (i ⊔ j) where
  ι₁ : A → A ⊎ B
  ι₂ : B → A ⊎ B

ind⊎ : ∀ {i j k} {A : Set i}{B : Set j}(P : A ⊎ B → Set k) →
  ((a : A) → P (ι₁ a)) → ((b : B) → P (ι₂ b)) →
  (t : A ⊎ B) → P t
ind⊎ P u v (ι₁ t) = u t
ind⊎ P u v (ι₂ t) = v t

case⊎ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} →
  A ⊎ B → (A → C) → (B → C) → C
case⊎ (ι₁ t) u v = u t
case⊎ (ι₂ t) u v = v t

record ↑p {i} (A : Prop i) : Set i where
  constructor ↑[_]↑
  field
    ↓[_]↓ : A
open ↑p public

*↑ : ↑p 𝟙
*↑ = ↑[ * ]↑

infix 3 ¬_
¬_ : ∀ {i} → Prop i → Prop i
¬ A = A → 𝟘

infixl 2 _×_
_×_ : ∀ {i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A (const B)

infixl 2 _×p_
_×p_ : ∀ {i j} → Prop i → Prop j → Prop (i ⊔ j)
A ×p B = Σp A (λ _ → B)

infix 1 _↔_
_↔_ : ∀ {i j} → Set i → Set j → Set (i ⊔ j)
A ↔ B = (A → B) × (B → A)

infix 4 _≡_
data _≡_ {i} {A : Set i}(x : A) : A → Prop i where
  refl : x ≡ x
{-# BUILTIN REWRITE _≡_ #-}

record _≅_ {i j} (A : Set i)(B : Set j) : Set (i ⊔ j) where
  field
    f : A → B
    g : B → A
    fg : ∀ {b} → f (g b) ≡ b
    gf : ∀ {a} → g (f a) ≡ a
open _≅_ public

refl↑ : ∀ {i} {A : Set i}{x : A} → ↑p (x ≡ x)
refl↑ = ↑[ refl ]↑

transportp : ∀ {i j} {A : Set i}(P : A → Prop j)
  {x y : A} → x ≡ y → P x → P y
transportp P refl u = u

postulate
  transport : ∀ {i j} {A : Set i}(P : A → Set j)
    {x y : A} → x ≡ y → P x → P y
  transport-refl : ∀ {i j} {A : Set i}{P : A → Set j}
    {x : A}{px : P x} → transport P refl px ≡ px
  {-# REWRITE transport-refl #-}

infix 3 _∎
_∎ : ∀ {i} {A : Set i}(x : A) → x ≡ x
x ∎ = refl

infix 5 _⁻¹
_⁻¹ : ∀ {i} {A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

infixr 4 _◾_
_◾_ : ∀ {i} {A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ p = p

infixr 2 _≡⟨_⟩_
_≡⟨_⟩_ : ∀{ℓ}{A : Set ℓ}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = x≡y ◾ y≡z

infixr 2 _≡≡_
_≡≡_ : ∀{ℓ}{A : Set ℓ}(x : A){y : A} → x ≡ y → x ≡ y
x ≡≡ x≡y = x≡y

ap : ∀ {i j} {A : Set i}{B : Set j}(f : A → B)
  {x y : A} → x ≡ y → f x ≡ f y
ap f refl = refl

infixl 2 _=$=_
_=$=_ : ∀ {i j} {A : Set i}{B : Set j}(f : A → B)
  {x y : A} → x ≡ y → f x ≡ f y
_=$=_ = ap  

ap2 : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k}(f : A → B → C)
  {x₁ y₁ : A}{x₂ y₂ : B} → x₁ ≡ y₁ → x₂ ≡ y₂ → f x₁ x₂ ≡ f y₁ y₂
ap2 f refl refl = refl

ap3 : ∀ {i j k l} {A : Set i}{B : Set j}{C : Set k}{D : Set l}
  (f : A → B → C → D){x₁ y₁ : A}{x₂ y₂ : B}{x₃ y₃ : C} →
  x₁ ≡ y₁ → x₂ ≡ y₂ → x₃ ≡ y₃ → f x₁ x₂ x₃ ≡ f y₁ y₂ y₃
ap3 f refl refl refl = refl


rail : ∀ {i} {A : Set i}{x y z w : A} →
  x ≡ z → y ≡ w → z ≡ w → x ≡ y
rail t b r = t ◾ r ◾ b ⁻¹

coe : ∀ {i} {A B : Set i} → A ≡ B → A → B
coe = transport idf

infix 4 _=[_]=_
_=[_]=_ : ∀ {i}{A B : Set i} → A → A ≡ B → B → Prop i
x =[ p ]= y = coe p x ≡ y

apd : ∀{i j}{A : Set i}{B : A → Set j}(f : (x : A) → B x){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ =[ ap B a₂ ]= f a₁
apd f refl = refl

data Maybe {i}(A : Set i) : Set i where
  Nothing  : Maybe A
  Just     : A → Maybe A
  

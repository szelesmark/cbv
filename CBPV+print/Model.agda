{-# OPTIONS --prop --rewriting #-}

module CBPV+print.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import CBPV+print.Algebra

-- the (List ℕ ×_) functor's action on sets
T : Set → Set
T A = List ℕ × A

-- the (List ℕ ×_) functor's action on functions
T₁ : ∀ {A B} → (A → B) → T A → T B
T₁ f ta = (π₁ ta) ,Σ f (π₂ ta)

-- monad unit
η : ∀ {A} → A → T A
η a = [] ,Σ a

-- monad multiplication
μ : ∀ {A} → T (T A) → T A
μ (ns ,Σ ta) = (ns ++ π₁ ta) ,Σ π₂ ta

-- ℕ* × (ℕ* × X)

-- tensorial strength
t : ∀ {A B} → A × T B → T (A × B)
t (a ,Σ (ns ,Σ b)) = ns ,Σ (a ,Σ b)

-- algebra over T
record T-algebra (X : Set) : Set₁ where
  field
    θ : T X → X
    ηθ : ∀ {x} → θ (η x) ≡ x
    μθ : ∀ {ttx} → θ (μ ttx) ≡ θ (T₁ θ ttx)
open T-algebra

postulate
  funext : ∀ {i}{A B : Set i}{f g : A → B} →
    ((a : A) → f a ≡ g a) → f ≡ g

++-ass : ∀ {i} {A : Set i} (ns ms ps : List A) → ns ++ ms ++ ps ≡ ns ++ (ms ++ ps)
++-ass [] ms ps = refl
++-ass (x ∷ ns) ms ps = ap (x ∷_) (++-ass ns ms ps)

[]-idr : ∀ {i} {A : Set i} (ns : List A) → ns ++ [] ≡ ns
[]-idr [] = refl
[]-idr (x ∷ ns) = ap (x ∷_) ([]-idr ns)

<<<<<<< HEAD
μ-ass : ∀ {A} {ttta : T (T (T A))} → μ (μ ttta) ≡ μ (T₁ μ ttta)
μ-ass {ttta = ttta} = ap2 _,Σ_
                          (++-ass (π₁ ttta) (π₁ (π₂ ttta)) (π₁ (π₂ (π₂ ttta))))
                          refl

tμ : ∀ {A B} {x : A × T (T B)} →
      μ (T₁ t (t x)) ≡ t (π₁ x ,Σ μ (π₂ x))
tμ {x = x} = refl
=======
α : ∀ {A B C : Set} → (A × B) × C → A × (B × C)
α (a ,Σ b ,Σ c) = a ,Σ (b ,Σ c)

t× : ∀ {A B C} {x : (A × B) × T C} →
  t (π₁ (α x) ,Σ t (π₂ (α x))) ≡ T₁ α (t x)
t× = refl
>>>>>>> 7bba0748c1c77a70b031c906cdac7be27dc6cb25

model : Algebra
model = record
  { Con = Set
  ; Sub = λ Γ Δ → Γ → Δ
  ; TyV = Set
  ; TyC = Σ Set T-algebra
  ; Val = λ Γ A → Γ → A
  ; Tm = λ Γ A → Γ → π₁ A
  ; U = π₁
  ; Bool = 𝟚
  ; _×_ = _×_
  ; F = λ X → T X ,Σ
              record
                { θ = μ
                ; ηθ = refl
                ; μθ = λ {ttx} → ap (_,Σ π₂ (π₂ (π₂ ttx)))
                                    (++-ass
                                    (π₁ ttx) (π₁ (π₂ ttx)) (π₁ (π₂ (π₂ ttx))))
                }
  ; _π_ = λ A B → (π₁ A × π₁ B) ,Σ
                  let module A = T-algebra (π₂ A); module B = T-algebra (π₂ B)
                  in record
                      { θ = λ tab → A.θ (T₁ π₁ tab) ,Σ B.θ (T₁ π₂ tab)
                      ; ηθ = ap2 _,Σ_ A.ηθ B.ηθ
                      ; μθ = ap2 _,Σ_ A.μθ B.μθ
                      }
  ; _⇒_ = λ A B → (A → (π₁ B)) ,Σ let module B = T-algebra (π₂ B)
                  in record
                      { θ = λ t_a-b a → B.θ (T₁ (λ f → f a) t_a-b)
                      ; ηθ = funext λ a → B.ηθ
                      ; μθ = funext λ a → B.μθ
                      }
  ; ∙ = ↑p 𝟙
  ; _▹_ = _×_
  ; id = idf
  ; _∘_ = _∘f_
  ; _,_ = λ σ a γ → σ γ ,Σ a γ
  ; p = π₁
  ; ε = const *↑
  ; _[_]v = _∘f_
  ; _[_]t = _∘f_
  ; q = π₂
  ; return = λ a γ → η (a γ)
  ; _to_ = λ {Γ A B} a u γ → θ (π₂ B) (T₁ u (t (γ ,Σ a γ)))
  ; print_then_ = λ {Γ A} n t γ → θ (π₂ A) (n ∷ [] ,Σ t γ)
  ; thunk = idf
  ; force = idf
  ; lam = λ t γ a → t (γ ,Σ a)
  ; app = λ t γa → t (π₁ γa) (π₂ γa)
  ; true = const I
  ; false = const O
  ; itev = λ b u v γ → if b γ then u γ else v γ
  ; itet = λ b u v γ → if b γ then u γ else v γ
  ; _,×_ = λ a b γ → a γ ,Σ b γ
  ; pm×v = λ ab t γ → t (γ ,Σ π₁ (ab γ) ,Σ π₂ (ab γ))
  ; pm×t = λ ab t γ → t (γ ,Σ π₁ (ab γ) ,Σ π₂ (ab γ))
  ; _,π_ = λ a b γ → a γ ,Σ b γ
  ; proj₁ = λ ab γ → π₁ (ab γ)
  ; proj₂ = λ ab γ → π₂ (ab γ)
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ∙η = refl
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; [id]v = refl
  ; [id]t = refl
  ; [∘]v = refl
  ; [∘]t = refl
  ; toβ = λ {_ _ B} → funext λ γ → ηθ (π₂ B)
  ; toη = λ {_ _ u} → funext λ γ → ap (_,Σ π₂ (u γ)) ([]-idr _)
<<<<<<< HEAD
  ; toass = λ {Γ A B C u v w} → funext λ γ → {!!}
  ; toπ = λ {Γ A B C u b w} → refl
  ; to⇒ = refl
  ; printβ = λ {_ _ B} → let module B = T-algebra (π₂ B)
                         in funext λ γ → B.μθ
=======
  ; toass = λ {Γ A B C u v w} → funext λ γ →
                 let module C = T-algebra (π₂ C) in
                 C.μθ
  ; toπ = λ {Γ A B C u b w} → refl
  ; to⇒ = refl
  ; printβ = λ {Γ A B n u v} → funext λ a → 
                  let module B = T-algebra (π₂ B)
                  in B.μθ  
>>>>>>> 7bba0748c1c77a70b031c906cdac7be27dc6cb25
  ; printπ = λ {Γ A B n u v} → refl
  ; print⇒ = refl
  ; Uβ = refl
  ; Uη = refl
  ; ⇒β = refl
  ; ⇒η = refl
  ; Boolβ₁v = refl
  ; Boolβ₂v = refl
  ; Boolβ₁t = refl
  ; Boolβ₂t = refl
  ; Boolη = λ {Γ A t} → funext λ γ → ind𝟚p
              (λ b → t (π₁ γ ,Σ b) ≡ (if b then t (π₁ γ ,Σ I) else t (π₁ γ ,Σ O)))
              refl refl (π₂ γ)
  ; ×βv = refl
  ; ×βt = refl
  ; ×ηv = refl
  ; ×ηt = refl
  ; πβ₁ = refl
  ; πβ₂ = refl
  ; πη = refl
  ; return[] = refl
  ; to[] = refl
  ; print[] = refl
  ; thunk[] = refl
  ; force[] = refl
  ; lam[] = refl
  ; app[] = refl
  ; true[] = refl
  ; false[] = refl
  ; ite[]v = refl
  ; ite[]t = refl
  ; ,×[] = refl
  ; pm×v[] = refl
  ; pm×t[] = refl
  ; ,π[] = refl
  ; proj₁[] = refl
  ; proj₂[] = refl
  }

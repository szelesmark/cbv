{-# OPTIONS --prop #-}

module DCBPV.Reader (Cell : Set) where

open import Lib renaming (_∘_ to _∘f_ ; Σ to Σm ; _,_ to _,Σm_)

infixl 5 _▹_
-- infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
infixl 7 _↑_
infixl 8 _[_]Tv
infixl 8 _[_]Tc
infixl 8 _[_]tv
infixl 8 _[_]tc

private

  record Con' (i : Level) : Set (lsuc i) where
    constructor mkCon
    field
     ∣_∣C : Set i
  open Con'

  record Sub' {i j}
    (Γ : Con' i)(Δ : Con' j) : Set (i ⊔ j) where
      constructor mkSub
      field
        ∣_∣S : ∣ Γ ∣C → ∣ Δ ∣C
  open Sub'

  record TyV' {i}(j : Level)
    (Γ : Con' i) : Set (i ⊔ lsuc j) where
    constructor mkTyV
    field
      ∣_∣TV : ∣ Γ ∣C → Set j
  open TyV'

  record TyC' {i}(j : Level)
    (Γ : Con' i) : Set (i ⊔ lsuc j) where
    constructor mkTyC
    field
      ∣_∣TC : ∣ Γ ∣C → Cell → Set j
  open TyC'

  record Val' {i j} (Γ : Con' i) (A : TyV' j Γ) : Set (i ⊔ j) where
    constructor mkVal
    field
      ∣_∣v : (γ : ∣ Γ ∣C) → ∣ A ∣TV γ 
  open Val'

  record Tm' {i j} (Γ : Con' i) (X : TyC' j Γ) : Set (i ⊔ j) where
    constructor mkTm
    field
      ∣_∣t : (γ : ∣ Γ ∣C) (c : Cell) → ∣ X ∣TC γ c 
  open Tm'

-------------------------|
-- sorts of the calculus |
-------------------------|

Con : (i : Level) → Set (lsuc i)
Con = Con' 

Sub : {i j : Level} → Con i → Con j → Set (i ⊔ j)
Sub = Sub'

TyV : {i : Level}(j : Level) → Con i → Set (i ⊔ lsuc j)
TyV = TyV'

TyC : {i : Level}(j : Level) → Con i → Set (i ⊔ lsuc j)
TyC = TyC'

Val : {i j : Level}(Γ : Con i) → TyV j Γ → Set (i ⊔ j)
Val = Val'

Tm : {i j : Level}(Γ : Con i) → TyC j Γ → Set (i ⊔ j)
Tm = Tm'


--------------------------------|
-- Sub identity and composition |
--------------------------------|

id : ∀ {i}{Γ : Con i} → Sub Γ Γ
id = mkSub idf

_∘_ : ∀ {i j k} {Γ : Con i} {Δ : Con j} {Θ : Con k} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
σ ∘ δ = mkSub λ γ → ∣ σ ∣S (∣ δ ∣S γ)

ass : ∀ {i j k l}{Γ : Con i} {Δ : Con j} {Θ : Con k} {Λ : Con l}
  {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

idl : ∀ {i j}{Γ : Con i} {Δ : Con j} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl

idr : ∀ {i j}{Γ : Con i} {Δ : Con j} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl

-----------------------------------|
-- substitution of types and terms |
-----------------------------------|

_[_]Tv : ∀ {i j k}{Γ : Con i} {Δ : Con j} → TyV k Δ → Sub Γ Δ → TyV k Γ
(A [ σ ]Tv) = mkTyV (λ γ → ∣ A ∣TV (∣ σ ∣S γ))

_[_]Tc : ∀ {i j k}{Γ : Con i} {Δ : Con j} → TyC k Δ → Sub Γ Δ → TyC k Γ
(X [ σ ]Tc) = mkTyC (λ γ → ∣ X ∣TC (∣ σ ∣S γ))

_[_]tv : ∀ {i j k}{Γ : Con i} {Δ : Con j} {A : TyV k Δ} →
  Val Δ A → (σ : Sub Γ Δ) → Val Γ (A [ σ ]Tv)
(v [ σ ]tv)  = mkVal (λ γ → ∣ v ∣v ( ∣ σ ∣S γ))

_[_]tc : ∀ {i j k}{Γ : Con i} {Δ : Con j} {A : TyC k Δ} →
  Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]Tc)
(t [ σ ]tc) = mkTm (λ γ → ∣ t ∣t ( ∣ σ ∣S γ))

[id]Tv : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → A [ id ]Tv ≡ A
[id]Tv = refl

[id]Tc : ∀ {i j}{Γ : Con i}{A : TyC j Γ} → A [ id ]Tc ≡ A
[id]Tc = refl

[∘]Tv : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyV l Θ}
  {σ : Sub Δ Θ}{δ : Sub Γ Δ} → A [ σ ]Tv [ δ ]Tv ≡ A [ σ ∘ δ ]Tv
[∘]Tv = refl

[∘]Tc : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyC l Θ}
  {σ : Sub Δ Θ}{δ : Sub Γ Δ} → A [ σ ]Tc [ δ ]Tc ≡ A [ σ ∘ δ ]Tc
[∘]Tc = refl

[id]tv : ∀ {i j}{Γ : Con i}{A : TyV j Γ}{t : Val Γ A} → t [ id ]tv ≡ t
[id]tv = refl

[∘]tv : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyV l Θ}
  {t : Val Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]tv [ δ ]tv ≡ t [ σ ∘ δ ]tv
[∘]tv = refl

[id]tc : ∀ {i j}{Γ : Con i}{A : TyC j Γ}{t : Tm Γ A} → t [ id ]tc ≡ t
[id]tc = refl

[∘]tc : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyC l Θ}
  {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]tc [ δ ]tc ≡ t [ σ ∘ δ ]tc
[∘]tc = refl

-----------------|
-- Contexts      |
-----------------|

∙ : Con lzero
∙ = mkCon (↑p 𝟙)

ε : ∀ {i}{Γ : Con i} → Sub Γ ∙
ε = mkSub (const *↑)

∙η : ∀ {i}{Γ : Con i}{σ : Sub Γ ∙} → σ ≡ ε
∙η = refl

_▹_ : ∀ {i j}(Γ : Con i) → TyV j Γ → Con (i ⊔ j)
Γ ▹ A = mkCon (Σm ∣ Γ ∣C ∣ A ∣TV)

_,_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}(σ : Sub Γ Δ) →
  Val Γ (A [ σ ]Tv) → Sub Γ (Δ ▹ A)
(σ , v) = mkSub λ γ → (∣ σ ∣S γ) ,Σm (∣ v ∣v γ)

p : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → Sub (Γ ▹ A) Γ
p = mkSub π₁

q : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → Val (Γ ▹ A) (A [ p ]Tv)
q = mkVal π₂

▻β₁ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}
  {σ : Sub Γ Δ}{t : Val Γ (A [ σ ]Tv)} → p {Γ = Δ}{A} ∘ (σ , t) ≡ σ
▻β₁ = refl

▻β₂ : ∀ {i j k}{Γ : Con i}{Δ : Con j} {A : TyV k Δ}
  {σ : Sub Γ Δ}{t : Val Γ (A [ σ ]Tv)} → q {Γ = Δ}{A} [ σ , t ]tv ≡ t
▻β₂ = refl

▹η : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}
  {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ]tv ≡ σ
▹η = refl

,∘ : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyV l Θ}
  {σ : Sub Δ Θ}{t : Val Δ (A [ σ ]Tv)}{δ : Sub Γ Δ} →
  (_,_ {A = A} σ t) ∘ δ ≡ σ ∘ δ , t [ δ ]tv
,∘ = refl

_↑_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}(σ : Sub Γ Δ)(A : TyV k Δ) →
  Sub (Γ ▹ A [ σ ]Tv) (Δ ▹ A)
σ ↑ A = σ ∘ p , q

--------------------------|
-- universes á la Coquand |
--------------------------|

□v : ∀{i}{Γ : Con i}(j : Level) → TyV (lsuc j) Γ
□v j = mkTyV λ γ → Set j

coquandv : ∀{i j : Level}{Γ : Con i} → Val Γ (□v i) ≅ TyV i Γ
coquandv {Γ = Γ} = record
  { f = λ v → mkTyV ∣ v ∣v
  ; g = λ A → mkVal ∣ A ∣TV
  ; fg = refl
  ; gf = refl
  }

□c : ∀{i}{Γ : Con i}(j : Level) → TyC (lsuc j) Γ
□c j = mkTyC (λ γ c → Set j)

coquandc : ∀{i j : Level}{Γ : Con i} → Tm Γ (□c j) ≅ TyC j Γ
coquandc = record { f = λ t → mkTyC ∣ t ∣t
                               ; g = λ X → mkTm ∣ X ∣TC
                               ; fg = refl
                               ; gf = refl }

ElC : ∀{i j : Level}{Γ : Con i} → TyC j Γ → Tm Γ (□c j)  
ElC {i}{j} {Γ} = g (coquandc {i}{j}{Γ})

cC :  ∀{i j : Level}{Γ : Con i} → Tm Γ (□c j) → TyC j Γ
cC {i}{j}{Γ} = f (coquandc {i}{j}{Γ})

-------------------------|
-- F type and sequencing |
-------------------------|

F : ∀ {i j}{Γ : Con i} → TyV j Γ → TyC j Γ
F A = mkTyC (λ γ c → ∣ A ∣TV γ)

return : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → Val Γ A → Tm Γ (F A)
return v = mkTm (λ γ c → ∣ v ∣v γ)

_to_ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k Γ} →
  Tm Γ (F A) → Tm (Γ ▹ A) (X [ p ]Tc) → Tm Γ X
t to x = mkTm (λ γ c → ∣ x ∣t (γ ,Σm (∣ t ∣t  γ c)) c)

_dto_ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k (Γ ▹ A)} →
  (t : Tm Γ (F A)) → Tm (Γ ▹ A) X → Tm Γ (cC (t to ElC X))
t dto x = mkTm (λ γ c → ∣ x ∣t (γ ,Σm (∣ t ∣t  γ c)) c)

toβ : ∀{i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k Γ}
  {v : Val Γ A}{t : Tm (Γ ▹ A) (X [ p ]Tc)} → return v to t ≡ t [ id , v ]tc
toβ = refl

dtoβ : ∀{i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k (Γ ▹ A)}
  {v : Val Γ A}{t : Tm (Γ ▹ A) X} → return v dto t ≡ t [ id , v ]tc
dtoβ = refl

toη : ∀ {i j}{Γ : Con i}{A : TyV j Γ}{t : Tm Γ (F A)} → t to return q ≡ t
toη = refl

dtoη : ∀ {i j}{Γ : Con i}{A : TyV j Γ}{t : Tm Γ (F A)} → t dto return q ≡ t
dtoη = refl

-------------------------|
-- U type                |
-------------------------|

U : ∀ {i j}{Γ : Con i} → TyC j Γ → TyV j Γ
U X = mkTyV (λ γ → (c : Cell) → ∣ X ∣TC γ c)

thunk : ∀ {i j}{Γ : Con i}{X : TyC j Γ} → Tm Γ X → Val Γ (U X)
thunk t = mkVal ∣ t ∣t

force : ∀ {i j}{Γ : Con i}{X : TyC j Γ} → Val Γ (U X) → Tm Γ X
force v = mkTm ∣ v ∣v 

Uβ : ∀ {i j}{Γ : Con i}{X : TyC j Γ}{t : Tm Γ X} → force (thunk t) ≡ t
Uβ = refl

Uη : ∀ {i j}{Γ : Con i}{X : TyC j Γ}{v : Val Γ (U X)} → thunk (force v) ≡ v
Uη = refl


------------------|
-- Π type         |
------------------|

Π : ∀ {i j k} {Γ : Con i}(A : TyV j Γ) → TyC k (Γ ▹ A) → TyC (j ⊔ k) Γ
Π A X = mkTyC λ γ c → (v : ∣ A ∣TV γ) → ∣ X ∣TC  (γ ,Σm v) c

lam : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k (Γ ▹ A)} →
  Tm (Γ ▹ A) X → Tm Γ (Π A X)
(lam t) = mkTm λ γ c a → ∣ t ∣t (γ ,Σm a) c

app : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k (Γ ▹ A)} →
  Tm Γ (Π A X) → Tm (Γ ▹ A) X
(app t) = mkTm λ γa c → ∣ t ∣t (π₁ γa) c (π₂ γa)


Πβ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k (Γ ▹ A)}
  {t : Tm (Γ ▹ A) X} → app (lam t) ≡ t
Πβ = refl

Πη : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k (Γ ▹ A)}
  {t : Tm Γ (Π A X)} → lam (app t) ≡ t
Πη = refl

Π[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}{X : TyC l (Δ ▹ A)}
  {σ : Sub Γ Δ} → (Π A X) [ σ ]Tc ≡ Π (A [ σ ]Tv) (X [ σ ↑ A ]Tc)
Π[] = refl

lam[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}{X : TyC l (Δ ▹ A)}
  {t : Tm (Δ ▹ A) X}{σ : Sub Γ Δ} → (lam t) [ σ ]tc ≡ lam (t [ σ ↑ A ]tc)
lam[] = refl

------------------|
-- Σ type         |
------------------|

Σ : ∀ {i j k} {Γ : Con i}(A : TyV j Γ) → TyV k (Γ ▹ A) → TyV (j ⊔ k) Γ
Σ A B = mkTyV λ γ → Σm (∣ A ∣TV  γ) λ a → ∣ B ∣TV (γ ,Σm a)

⟨_,_⟩ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{B : TyV k (Γ ▹ A)}
  (u : Val Γ A) → Val Γ (B [ id , u ]Tv) → Val Γ (Σ A B)
⟨ u , v ⟩ = mkVal λ γ → ∣ u ∣v γ ,Σm ∣ v ∣v γ

proj₁ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{B : TyV k (Γ ▹ A)} →
  Val Γ (Σ A B) → Val Γ A
(proj₁ t) = mkVal λ γ → π₁ (∣ t ∣v γ)

proj₂ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{B : TyV k (Γ ▹ A)}
  (t : Val Γ (Σ A B)) → Val Γ (B [ id , proj₁ t ]Tv)
(proj₂ t) = mkVal λ γ → π₂ (∣ t ∣v γ)


Σβ₁ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{B : TyV k (Γ ▹ A)}
  {u : Val Γ A}{v : Val Γ (B [ id , u ]Tv)} → proj₁ {B = B} ⟨ u , v ⟩ ≡ u
Σβ₁ = refl

Σβ₂ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{B : TyV k (Γ ▹ A)}
  {u : Val Γ A}{v : Val Γ (B [ id , u ]Tv)} → proj₂ {B = B} ⟨ u , v ⟩ ≡ v
Σβ₂ = refl

Ση : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{B : TyV k (Γ ▹ A)}
  {t : Val Γ (Σ A B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
Ση = refl

Σ[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}{B : TyV l (Δ ▹ A)}
  {σ : Sub Γ Δ} → (Σ A B) [ σ ]Tv ≡ Σ (A [ σ ]Tv) (B [ σ ↑ A ]Tv)
Σ[] = refl

⟨,⟩[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}{B : TyV l (Δ ▹ A)}
  {u : Val Δ A}{v : Val Δ (B [ id , u ]Tv)}{σ : Sub Γ Δ} →
  (⟨_,_⟩ {B = B} u v) [ σ ]tv ≡ ⟨ u [ σ ]tv , v [ σ ]tv ⟩
⟨,⟩[] = refl

proj₁[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}{B : TyV l (Δ ▹ A)}
  {t : Val Δ (Σ A B)}{σ : Sub Γ Δ} → (proj₁ t) [ σ ]tv ≡ proj₁ (t [ σ ]tv)
proj₁[] = refl

proj₂[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}{B : TyV l (Δ ▹ A)}
  {t : Val Δ (Σ A B)}{σ : Sub Γ Δ} → (proj₂ t) [ σ ]tv ≡ proj₂ (t [ σ ]tv)
proj₂[] = refl

{-# OPTIONS --prop #-}

module DCBPV.DCBPV+print where

open import Lib renaming (_∘_ to _∘f_ ; Σ to Σm ; _,_ to _,Σm_)

infixl 5 _▹_
-- infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
-- infixl 7 _↑_
infixl 8 _[_]Tv
infixl 8 _[_]Tc
infixl 8 _[_]tv
infixl 8 _[_]tc

T : ∀{i} → Set i → Set i
T A = List ℕ × A

-- the (List ℕ ×_) functor's action on functions
T₁ : ∀ {i j}{A : Set i}{B : Set j} → (A → B) → T A → T B
T₁ f ta = (π₁ ta) ,Σm f (π₂ ta)

-- monad unit
η : ∀ {i}{A : Set i} → A → T A
η a = [] ,Σm a

-- monad multiplication
μ : ∀ {i}{A : Set i} → T (T A) → T A
μ (ns ,Σm ta) = (ns ++ π₁ ta) ,Σm π₂ ta

-- tensorial strength
t : ∀ {i j}{A : Set i} {B : Set j} → A × T B → T (A × B)
t (a ,Σm (ns ,Σm b)) = ns ,Σm (a ,Σm b)

++-ass : ∀ {i} {A : Set i} (ns ms ps : List A) → ns ++ ms ++ ps ≡ ns ++ (ms ++ ps)
++-ass [] ms ps = refl
++-ass (x ∷ ns) ms ps = ap (x ∷_) (++-ass ns ms ps)

private

  postulate
    funext : ∀ {i}{A B : Set i}{f g : A → B} →
      ((a : A) → f a ≡ g a) → f ≡ g

  record Con' (i : Level) : Set (lsuc i) where
    constructor mkCon
    field
      ∣_∣C : Set i
  open Con'

  record T-algebra {i j} (Γ : Con' i) : Set (i ⊔ lsuc j) where
    constructor mkTAlg
    field
      ∣_∣ : ∣ Γ ∣C → Set j
      θ : (γ : ∣ Γ ∣C) → T (∣ γ ∣) → ∣ γ ∣
      ηθ : ∀ {γ} {x : ∣ γ ∣} → θ γ (η x) ≡ x
      μθ : ∀ {γ} {ttx : T ( T (∣ γ ∣))} → θ γ (μ ttx) ≡ θ γ (T₁ (θ γ) ttx)
  open T-algebra

  record Sub' {i j} (Γ : Con' i)(Δ : Con' j) : Set (i ⊔ j) where
    constructor mkSub
    field
      ∣_∣S : ∣ Γ ∣C → ∣ Δ ∣C 
  open Sub'

  record TyV' {i} (j : Level) (Γ : Con' i) : Set (i ⊔ lsuc j) where
    constructor mkTyV
    field
      ∣_∣TV : ∣ Γ ∣C → Set j
  open TyV'

  record TyC' {i} (j : Level) (Γ : Con' i) : Set (i ⊔ lsuc j) where
    constructor mkTyC
    field
      ∣_∣TC : T-algebra {i}{j} Γ
  open TyC' 

  record Val' {i j : Level} (Γ : Con' i) (A : TyV' j Γ) : Set (i ⊔ j) where
    constructor mkVal
    field
      ∣_∣v : (γ : ∣ Γ ∣C) → ∣ A ∣TV γ
  open Val'

  record Tm' {i j} (Γ : Con' i) (X : TyC' j Γ) : Set (i ⊔ j) where
    constructor mkTm
    field
      ∣_∣t : (γ : ∣ Γ ∣C) → ∣ ∣ X ∣TC ∣ γ 
  open Tm'


-------------------------|
-- sorts of the calculus |
-------------------------|

Con : (i : Level) → Set (lsuc i)
Con = Con'

Sub : {i j : Level} → Con i → Con j → Set (i ⊔ j)
Sub = Sub'

TyV : {i : Level}(j : Level) → Con i → Set (i ⊔ lsuc j)
TyV = TyV'

TyC : {i : Level}(j : Level) → (Γ : Con i) → Set (i ⊔ lsuc j)
TyC = TyC'

Val : {i j : Level}(Γ : Con i) → TyV j Γ → Set (i ⊔ j)
Val = Val'

Tm : {i j : Level}(Γ : Con i) → TyC j Γ → Set (i ⊔ j)
Tm = Tm'


--------------------------------|
-- Sub identity and composition |
--------------------------------|

id : ∀ {i}{Γ : Con i} → Sub Γ Γ
id = mkSub idf

_∘_ : ∀ {i j k} {Γ : Con i} {Δ : Con j} {Θ : Con k} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
σ ∘ δ  =  mkSub λ γ → ∣ σ ∣S (∣ δ ∣S γ)

ass : ∀ {i j k l}{Γ : Con i} {Δ : Con j} {Θ : Con k} {Λ : Con l}
  {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

idl : ∀ {i j}{Γ : Con i} {Δ : Con j} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl

idr : ∀ {i j}{Γ : Con i} {Δ : Con j} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl

-----------------------------------|
-- substitution of types and terms |
-----------------------------------|

_[_]Tv : ∀ {i j k}{Γ : Con i} {Δ : Con j} → TyV k Δ → Sub Γ Δ → TyV k Γ
(A [ σ ]Tv) = mkTyV (λ γ → ∣ A ∣TV  (∣ σ ∣S γ))

_[_]Tc : ∀ {i j k}{Γ : Con i} {Δ : Con j} → TyC k Δ → Sub Γ Δ → TyC k Γ
(X [ σ ]Tc) = mkTyC (mkTAlg (λ γ → ∣ ∣ X ∣TC ∣ (∣ σ ∣S γ))
                                                       (λ γ tx → θ ∣ X ∣TC (∣ σ ∣S γ) tx)
                                                       (ηθ ∣ X ∣TC)
                                                       (μθ ∣ X ∣TC))

_[_]tv : ∀ {i j k}{Γ : Con i} {Δ : Con j} {A : TyV k Δ} →
  Val Δ A → (σ : Sub Γ Δ) → Val Γ (A [ σ ]Tv)
v [ σ ]tv = mkVal (λ γ → ∣ v ∣v (∣ σ ∣S γ)) 

_[_]tc : ∀ {i j k}{Γ : Con i} {Δ : Con j} {A : TyC k Δ} →
  Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]Tc)
t [ σ ]tc = mkTm (λ γ → ∣ t ∣t (∣ σ ∣S γ) )

[id]Tv : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → A [ id ]Tv ≡ A
[id]Tv = refl

[id]Tc : ∀ {i j}{Γ : Con i}{A : TyC j Γ} → A [ id ]Tc ≡ A
[id]Tc = refl

[∘]Tv : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyV l Θ}
  {σ : Sub Δ Θ}{δ : Sub Γ Δ} → A [ σ ]Tv [ δ ]Tv ≡ A [ σ ∘ δ ]Tv
[∘]Tv = refl

[∘]Tc : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyC l Θ}
  {σ : Sub Δ Θ}{δ : Sub Γ Δ} → A [ σ ]Tc [ δ ]Tc ≡ A [ σ ∘ δ ]Tc
[∘]Tc = refl

[id]tv : ∀ {i j}{Γ : Con i}{A : TyV j Γ}{t : Val Γ A} → t [ id ]tv ≡ t
[id]tv = refl

[∘]tv : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyV l Θ}
  {t : Val Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]tv [ δ ]tv ≡ t [ σ ∘ δ ]tv
[∘]tv = refl

[id]tc : ∀ {i j}{Γ : Con i}{A : TyC j Γ}{t : Tm Γ A} → t [ id ]tc ≡ t
[id]tc = refl

[∘]tc : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyC l Θ}
  {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]tc [ δ ]tc ≡ t [ σ ∘ δ ]tc
[∘]tc = refl

-----------------|
-- Contexts      |
-----------------|

∙ : Con lzero
∙ = mkCon (↑p 𝟙)

ε : ∀ {i}{Γ : Con i} → Sub Γ ∙
ε = mkSub (const *↑)

∙η : ∀ {i}{Γ : Con i}{σ : Sub Γ ∙} → σ ≡ ε
∙η = refl

_▹_ : ∀ {i j}(Γ : Con i) → TyV j Γ → Con (i ⊔ j)
Γ ▹ A = mkCon (Σm ∣ Γ ∣C  ∣ A ∣TV)

_,_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}(σ : Sub Γ Δ) →
  Val Γ (A [ σ ]Tv) → Sub Γ (Δ ▹ A)
(σ , t) = mkSub (λ γ → (∣ σ ∣S γ) ,Σm ∣ t ∣v γ)

p : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → Sub (Γ ▹ A) Γ
p = mkSub π₁

q : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → Val (Γ ▹ A) (A [ p ]Tv)
q = mkVal π₂


▹β₁ : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}
  {σ : Sub Γ Δ}{t : Val Γ (A [ σ ]Tv)} → p {Γ = Δ}{A} ∘ (σ , t) ≡ σ
▹β₁ = refl

▹β₂ : ∀ {i j k}{Γ : Con i}{Δ : Con j} {A : TyV k Δ}
  {σ : Sub Γ Δ}{t : Val Γ (A [ σ ]Tv)} → q {Γ = Δ}{A} [ σ , t ]tv ≡ t
▹β₂ = refl

▹η : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : TyV k Δ}
  {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ]tv ≡ σ
▹η = refl

,∘ : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : TyV l Θ}
  {σ : Sub Δ Θ}{t : Val Δ (A [ σ ]Tv)}{δ : Sub Γ Δ} →
  (_,_ {A = A} σ t) ∘ δ ≡ σ ∘ δ , t [ δ ]tv
,∘ = refl

_↑_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}(σ : Sub Γ Δ)(A : TyV k Δ) →
  Sub (Γ ▹ A [ σ ]Tv) (Δ ▹ A)
σ ↑ A = σ  ∘ p  , q

-------------------------|
-- F type and sequencing |
-------------------------|

F : ∀ {i j}{Γ : Con i} → TyV j Γ → TyC j Γ
F A = mkTyC (mkTAlg (λ γ → T (∣ A ∣TV γ))
                                  (λ γ → μ)
                                  refl
                                  λ {_ ttx} →  ap (_,Σm π₂ (π₂ (π₂ ttx)))
                                                          (++-ass
                                                         (π₁ ttx) (π₁ (π₂ ttx)) (π₁ (π₂ (π₂ ttx)))))

return : ∀ {i j}{Γ : Con i}{A : TyV j Γ} → Val Γ A → Tm Γ (F A)
return v = mkTm (λ γ → [] ,Σm ∣ v ∣v γ)

_to_ : ∀ {i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k Γ} →
  Tm Γ (F A) → Tm (Γ ▹ A) (X [ p ]Tc) → Tm Γ X
_to_ {X = X} a u = mkTm (λ γ →
        θ ∣ X ∣TC γ {!T₁ ∣ u ∣t!})

toβ : ∀{i j k}{Γ : Con i}{A : TyV j Γ}{X : TyC k Γ}
  {v : Val Γ A}{t : Tm (Γ ▹ A) (X [ p ]Tc)} → return v to t ≡ t [ id , v ]tc
toβ = {!!}

--------------------------|
-- universes á la Coquand |
--------------------------|

□v : ∀{i}{Γ : Con i}(j : Level) → TyV (lsuc j) Γ
□v j = mkTyV (λ γ → Set j)

□c : ∀{i}{Γ : Con i}(j : Level) → TyC (lsuc j) Γ
□c j = mkTyC (mkTAlg (λ γ → T (Set j))
                                    (λ γ → μ)
                                      refl
                                      λ {_ ttx} →  ap (_,Σm π₂ (π₂ (π₂ ttx)))
                                                          (++-ass
                                                         (π₁ ttx) (π₁ (π₂ ttx)) (π₁ (π₂ (π₂ ttx)))) )

coquandv : ∀{i j : Level}{Γ : Con i} → Val Γ (□v i) ≅ TyV i Γ
coquandv = record { f = λ v → mkTyV (λ γ → ∣ v ∣v γ)
                               ; g = λ A → mkVal (λ γ → ∣ A ∣TV γ )
                               ; fg = refl
                               ; gf = refl }

coquandt : ∀{i j : Level}{Γ : Con i} → Tm Γ (□c i) ≅ TyC i Γ
coquandt {i} = {!!}
